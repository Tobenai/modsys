using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Modsys
{
    [System.Serializable]
    public class Link<T> : BaseLink
    {
        [System.Serializable]
        public class Reference
        {
            [SerializeField] private Link<T> _linkRef;

            public void Invoke(T value)
            {
                _linkRef.Invoke(value);
            }
        }


        public delegate void InvokeEventHandler(T value);
        public event InvokeEventHandler InvokeEvent;
        [SerializeField] private T _value;
        public T Value { get { return _value; } set { _value = value; Invoke(value); } }

#if UNITY_EDITOR
        [SerializeField] [HideInInspector] private T _testValue;
        private void TestInvoke()
        {
            if (InvokeEvent != null)
                InvokeEvent(_testValue);
        }
#endif

        private void Invoke(T value)
        {
            if (InvokeEvent != null)
                InvokeEvent(value);
        }
    }
}
