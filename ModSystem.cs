using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.Reflection;
using System.Linq;
using Sirenix.OdinInspector;

namespace Modsys
{
    public abstract class ModSystem : MonoBehaviour, ISerializationCallbackReceiver
    {
        public List<ModComponent> ModComponents => _modComponents;
        public List<string> ModFieldNames => _modFieldNames;

        [SerializeField] [HideInInspector] private List<ModComponent> _modComponents;
        [SerializeField] [HideInInspector] private List<string> _modFieldNames;

        public void OnAfterDeserialize()
        {
        }

        public void OnBeforeSerialize()
        {
            _modComponents.Clear();
            _modFieldNames.Clear();
            var components = GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance).Where(x => x.FieldType.IsSubclassOf(typeof(ModComponent)) && !x.FieldType.IsAbstract);
            foreach (var component in components)
            {
                if (component != null)
                    _modComponents.Add(component.GetValue(this) as ModComponent);
                _modFieldNames.Add(component.Name);
            }
        }

#if UNITY_EDITOR
        [ContextMenu("Edit Components")]
        private void EditSystem()
        {
            foreach (var component in _modComponents)
                AssetDatabase.OpenAsset(MonoScript.FromScriptableObject(component).GetInstanceID());
        }
#endif
    }
}


