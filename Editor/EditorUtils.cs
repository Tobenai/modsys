using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Modsys.Editor
{
    public static class EditorUtils
    {
        public static Texture2D MakeTex(Color col)
        {
            Color[] pix = new Color[600 * 1];

            for (int i = 0; i < pix.Length; i++)
                pix[i] = col;

            Texture2D result = new Texture2D(600, 1);
            result.SetPixels(pix);
            result.Apply();

            return result;
        }
    }
}


