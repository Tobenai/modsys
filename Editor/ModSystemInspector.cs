using Modsys;
using Sirenix.OdinInspector.Editor;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.Reflection;
using System.Linq;
using Sirenix.Utilities.Editor;
using System.ComponentModel;
using Sirenix.OdinInspector;

[CustomEditor(typeof(ModSystem), true)]
public class ModSystemInspector : OdinEditor
{
    private Dictionary<string, List<InspectorProperty>> _childTrees = new Dictionary<string, List<InspectorProperty>>();
    private Dictionary<InspectorProperty, string> _linkNames = new Dictionary<InspectorProperty, string>();
    private bool _isVisible;
    private bool _isEditable;
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        var tree = this.Tree;
        var obj = this.target as ModSystem;
        if (SirenixEditorGUI.BeginToggleGroup(obj.name, ref _isEditable, ref _isVisible, "Debug Values"))
        {
            foreach (var fieldName in obj.ModFieldNames)
            {
                var componentProp = tree.GetPropertyAtPath(fieldName);
                if (!_childTrees.ContainsKey(componentProp.Name) || obj.GetType().GetField(fieldName, BindingFlags.NonPublic | BindingFlags.Instance).GetValue(obj) == null)
                {
                    if (componentProp.BaseValueEntry.WeakSmartValue == null)
                    {
                        _childTrees.Remove(componentProp.Name);
                        continue;
                    }
                    var componentTree = PropertyTree.Create(componentProp.BaseValueEntry.WeakSmartValue);
                    TypeDescriptor.AddAttributes(componentProp.BaseValueEntry.WeakSmartValue, new AssetListAttribute());
                    List<InspectorProperty> linkProps = new List<InspectorProperty>();
                    foreach (var linkType in componentTree.TargetType.GetFields(BindingFlags.NonPublic | BindingFlags.Instance).Where(x => x.FieldType.IsSubclassOf(typeof(BaseLink))))
                    {
                        var linkTree = PropertyTree.Create(linkType.GetValue(componentProp.BaseValueEntry.WeakSmartValue));
                        var linkProp = linkTree.GetPropertyAtPath("_value");
                        linkProps.Add(linkProp);
                        _linkNames.Add(linkProp, ObjectNames.NicifyVariableName(linkType.Name));
                    }
                    _childTrees.Add(componentProp.Name, linkProps);
                }
                SirenixEditorGUI.BeginBox(ObjectNames.NicifyVariableName(fieldName));
                if (_childTrees.ContainsKey(componentProp.Name))
                {
                    foreach (var prop in _childTrees[componentProp.Name])
                    {
                        prop.Label = new GUIContent(_linkNames[prop]);
                        prop.Tree.Draw();
                    }
                }
                SirenixEditorGUI.EndBox();
            }
        }
        SirenixEditorGUI.EndToggleGroup();
    }
}
