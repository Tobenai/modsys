using System;
using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace Modsys.Editor
{
    public class LinkGeneratorEditor : EditorWindow
    {
        static readonly Dictionary<String, String> typeKeywordMap = new Dictionary<String, String>()
{
    { "String", "String" },
    { "SByte", "Sbyte" },
    { "Byte", "Byte" },
    { "Int16", "Short" },
    { "UInt16", "Ushort" },
    { "Int32", "Int" },
    { "UInt32", "Uint" },
    { "Int64", "Long" },
    { "UInt64", "Ulong" },
    { "Char", "Char" },
    { "Single", "Float" },
    { "Double", "Double" },
    { "Boolean", "Bool" },
    { "Decimal", "Decimal" },
    { "Void", "void" },
    { "Object", "Object" }
};

        [MenuItem("Assets/Create/Modsys/New Link", priority = 0)]
        private static void CreateEvent()
        {
            GetWindow<LinkGeneratorEditor>().InitSearchWindow();
        }

        private string _search = "";
        private Vector2 _scroll;
        private void OnGUI()
        {
            _search = GUILayout.TextField(_search, GUI.skin.FindStyle("ToolbarSeachTextField"));
            _scroll = EditorGUILayout.BeginScrollView(_scroll);

            DisplayTypesAsButtons(Assembly.GetExecutingAssembly().GetTypes());
            Assembly mscorlib = typeof(string).Assembly;
            DisplayTypesAsButtons(mscorlib.GetTypes());
            mscorlib = typeof(Vector3).Assembly;
            DisplayTypesAsButtons(mscorlib.GetTypes());
            EditorGUILayout.EndScrollView();
        }

        private void DisplayTypesAsButtons(IEnumerable<Type> types)
        {
            foreach (Type type in types.Where(x => GetTypeKeyword(x.Name).ToLower().Contains(_search.ToLower())))
            {
                string name = GetTypeKeyword(type.Name);
                if (GUILayout.Button(name))
                {
                    LinkGenerator.CreateLinkClass(type);
                    string path = AssetDatabase.GetAssetPath(Selection.activeObject);
                    if (path == "")
                    {
                        path = "Assets";
                    }
                    else if (Path.GetExtension(path) != "")
                    {
                        path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
                    }
                    var asset = ScriptableObject.CreateInstance($"{type.Name}Event");
                    ProjectWindowUtil.CreateAsset(asset, AssetDatabase.GenerateUniqueAssetPath(path + $"/New {name}Event.asset"));
                }
            }
        }

        private string GetTypeKeyword(string type, bool toLower = false)
        {
            if (typeKeywordMap.ContainsKey(type))
                return toLower ? typeKeywordMap[type].ToLower() : typeKeywordMap[type];
            //Don't ToLower() this one ploz
            return type;
        }

        public void InitSearchWindow()
        {
            Show();
        }
    }
}


