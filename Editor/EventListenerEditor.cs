using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace Modsys.Editor
{
    //[CustomEditor(typeof(EventListener))]
    public class EventListenerEditor : UnityEditor.Editor
    {
        private SerializedProperty _eventListProp;

        private void OnEnable()
        {
            _eventListProp = serializedObject.FindProperty("_eventListeners");
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            var style = new GUIStyle();
            style.normal.background = EditorUtils.MakeTex(Color.gray);
            for (int i = 0; i < _eventListProp.arraySize; i++)
            {
                EditorGUILayout.BeginVertical(style);
                EditorGUILayout.BeginHorizontal();
                if (GUILayout.Button("X", GUILayout.Width(20)))
                {
                    _eventListProp.DeleteArrayElementAtIndex(i);
                    serializedObject.ApplyModifiedProperties();
                    return;
                }

                EditorGUILayout.EndHorizontal();
                EditorGUILayout.BeginHorizontal();
                var linkProp = _eventListProp.GetArrayElementAtIndex(i).FindPropertyRelative("_link");
                var linkObj = linkProp.objectReferenceValue;
                if (linkObj != null)
                {
                    var targetObjProp = _eventListProp.GetArrayElementAtIndex(i).FindPropertyRelative("_targetObj");
                    //if (targetObjProp.objectReferenceValue == null)
                    //    targetObjProp.objectReferenceValue = (target as MonoBehaviour).gameObject;
                    EditorGUILayout.PropertyField(targetObjProp, GUIContent.none);
                    Type type = linkObj.GetType();
                    var components = (targetObjProp.objectReferenceValue as GameObject)?.GetComponents<Component>();
                    string[] methodList = GetMethodsFromObject(targetObjProp.objectReferenceValue, components, linkObj.GetType().BaseType.GetGenericArguments()[0]);
                    if (methodList == null)
                        methodList = new string[] { "No Methods Found" };
                    int methodIndex = EditorGUILayout.Popup(0, methodList);
                    var methodNameProp = _eventListProp.GetArrayElementAtIndex(i).FindPropertyRelative("_methodName");
                    string[] methodNameList = methodList[methodIndex].Split('/');
                    if (methodNameList.Length > 1)
                        methodNameProp.stringValue = methodNameList[1].Split(' ')[0];
                    var targetMethodObjProp = _eventListProp.GetArrayElementAtIndex(i).FindPropertyRelative("_targetMethodObj");
                    if (components == null)
                        targetMethodObjProp.objectReferenceValue = targetObjProp.objectReferenceValue;
                    else
                    {
                        string compName = methodList[methodIndex].Split('/')[0];
                        targetMethodObjProp.objectReferenceValue = components.FirstOrDefault(x => x.GetType().Name == compName);
                    }
                }
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.EndVertical();
                EditorGUILayout.Space();
            }

            if (GUILayout.Button("Add Listener"))
            {
                _eventListProp.arraySize++;
            }

            serializedObject.ApplyModifiedProperties();
        }

        private string[] GetMethodsFromObject(UnityEngine.Object obj, Component[] components, Type type)
        {
            if (obj == null)
                return null;
            var gameObj = obj as GameObject;
            if (gameObj == null)
                return obj.GetType().GetMethods(BindingFlags.Public | BindingFlags.Instance).Where(x => x.GetParameters().Length == 1 && x.GetParameters()[0].ParameterType == type).Select(x => x.Name).ToArray();
            else
            {
                var list = new List<string>();
                foreach (Component comp in gameObj.GetComponents<Component>())
                {
                    list.AddRange(comp.GetType().GetMethods(BindingFlags.Public | BindingFlags.Instance).Where(x => x.GetParameters().Length == 1 && x.GetParameters()[0].ParameterType == type).Select(x => comp.GetType().Name + "/" + x.Name));
                }
                return list.ToArray();
            }
        }
    }
}
