using Sirenix.OdinInspector;
using Sirenix.Utilities.Editor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace Modsys
{
    public class EventListener : MonoBehaviour
    {
        [System.Serializable]
        public class Instance
        {
            [AssetSelector]
            [HideLabel]
            [HideReferenceObjectPicker]
            [SerializeField] private BaseLink _link;

            [HorizontalGroup]
            [ShowIf("_link")] [HideLabel] [AssetSelector]
            [SerializeField] private UnityEngine.Object _targetObj;

            [HorizontalGroup]
            [CustomValueDrawer("CustomObjectPicker")] [ShowIf("_targetObj")]
            [SerializeField] private UnityEngine.Object _targetMethodObj;

            [SerializeField] [HideInInspector] private string _methodName;

            private UnityEngine.Object CustomObjectPicker(UnityEngine.Object value, GUIContent label)
            {
                var gameObject = (_targetObj as GameObject);

                var components = (_targetObj as GameObject)?.GetComponents<Component>();

                List<string> methodList = GetMethodsFromObject(_targetObj, components, _link.GetType().BaseType.GetGenericArguments()[0]).ToList();

                if (methodList == null || methodList.Count() == 0)
                    methodList = new List<string> { "No Methods Found" };

                int methodIndex = methodList.IndexOf(_targetMethodObj?.GetType().Name + "/" + _methodName);
                methodIndex = methodIndex == -1 ? 0 : methodIndex;

                string[] currentObjMethod = methodList[methodIndex].Split('/');
                _targetMethodObj = gameObject != null ? gameObject.GetComponent(currentObjMethod[0]) : _targetObj;
                _methodName = currentObjMethod[1];

                methodIndex = EditorGUILayout.Popup(methodIndex, methodList.ToArray());

                string[] methodNameList = methodList[methodIndex].Split('/');

                if (methodNameList.Length > 1)
                    _methodName = methodNameList[1].Split(' ')[0];

                return value;
            }

            private string[] GetMethodsFromObject(UnityEngine.Object obj, Component[] components, Type type)
            {
                if (obj == null)
                    return null;
                var gameObj = obj as GameObject;
                if (gameObj == null)
                    return obj.GetType().GetMethods(BindingFlags.Public | BindingFlags.Instance).Where(x => x.GetParameters().Length == 1 && x.GetParameters()[0].ParameterType == type).Select(x => obj.name + "/" + x.Name).ToArray();
                else
                {
                    var list = new List<string>();
                    foreach (Component comp in components)
                    {
                        list.AddRange(comp.GetType().GetMethods(BindingFlags.Public | BindingFlags.Instance).Where(x => x.GetParameters().Length == 1 && x.GetParameters()[0].ParameterType == type).Select(x => comp.GetType().Name + "/" + x.Name));
                    }
                    return list.ToArray();
                }
            }

            public void Init()
            {
                MethodInfo method = _targetMethodObj.GetType().GetMethod(_methodName);
                if (method == null)
                    return;
                EventInfo evInvoke = _link.GetType().GetEvent("InvokeEvent");
                Type tDelegate = evInvoke.EventHandlerType;
                Delegate d = Delegate.CreateDelegate(tDelegate, _targetMethodObj, method);

                MethodInfo addHandler = evInvoke.GetAddMethod();
                object[] addHandlerArgs = { d };
                addHandler.Invoke(_link, addHandlerArgs);
            }
        }
        [SerializeField] private List<Instance> _eventListeners;

        private void Awake()
        {
            foreach (Instance eli in _eventListeners)
                eli.Init();
        }
    }
}

