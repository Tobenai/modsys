using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;
using System.Linq;
using UnityEditor;
using Modsys;
using Modsys.Editor;

namespace Modsys
{
    public abstract class ModComponent : ScriptableObject, ISerializationCallbackReceiver
    {
        public void OnAfterDeserialize()
        {
        }

        [SerializeField] private bool _waitForNextSerialize = true;
        [SerializeField] private bool _hasSerialized = false;
        public void OnBeforeSerialize()
        {
#if UNITY_EDITOR
            if (_hasSerialized)
                return;
            if (!EditorUtility.IsPersistent(this))
                return;
            //This is because if we don't wait Unity tries to allocate 2TB of memory for some reason :<
            if (_waitForNextSerialize)
            {
                _waitForNextSerialize = false;
                return;
            }    
            foreach (var field in GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance)
                    .Where(x => x.FieldType.IsSubclassOf(typeof(BaseLink))))
            {
                var linkType = field.FieldType;
                var linkRef = field.GetValue(this);
                if (typeof(BaseLink).IsAssignableFrom(linkType))
                {
                    if (linkRef == null)
                    {
                        UnityEngine.Object[] subAssets = AssetDatabase.LoadAllAssetRepresentationsAtPath(AssetDatabase.GetAssetPath(this));
                        string displayName = ObjectNames.NicifyVariableName(field.Name);
                        Type linkValueType = linkType.GetGenericArguments()[0];
                        LinkGenerator.CreateLinkClass(linkValueType);
                        if (EditorApplication.isCompiling)
                        {
                            EditorUtility.DisplayProgressBar("Generating Links", $"Generating {linkValueType.Name} Link", -1);
                        }

                        var linkInst = ScriptableObject.CreateInstance($"{linkValueType.Name}Link");
                        linkInst.name = displayName;
                        AssetDatabase.AddObjectToAsset(linkInst, AssetDatabase.GetAssetPath(this));
                        AssetDatabase.Refresh();
                        field.SetValue(this, linkInst);
                    }
                    EditorUtility.ClearProgressBar();
                }
            }
            _hasSerialized = true;
#endif
        }
    }
}
