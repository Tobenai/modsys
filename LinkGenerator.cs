using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace Modsys.Editor
{
    public class LinkGenerator
    {
        private static string _template = @"
        using NAMESPACE;
        namespace Modsys.GeneratedLinks
        {
            public class TYPELink : Link<TYPE> {}
        }
        ";

        public static void CreateLinkClass(Type type)
        {
            string assetsDirPath = Path.Combine(Application.dataPath, "GeneratedLinks", $"{type.Name}Link.cs");
            if (!File.Exists(assetsDirPath))
            {
                string templateInst = _template;
                templateInst = templateInst.Replace("NAMESPACE", type.Namespace)
                                           .Replace("TYPE", type.Name);
                if (!Directory.Exists(Path.GetDirectoryName(assetsDirPath)))
                    Directory.CreateDirectory(Path.GetDirectoryName(assetsDirPath));
                File.WriteAllText(assetsDirPath, templateInst);
                AssetDatabase.Refresh();
            }
        }
    }
}

